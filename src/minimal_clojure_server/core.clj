(ns minimal-clojure-server.core
  (:require
   [ring.adapter.jetty :refer [run-jetty]]
   [clojure.string :as str]
   [clojure.tools.cli :as cli]))

(def cli-options
  [["-b" "--body-file BODY_FILE" "A json file to serve as body."
    :validate-fn (comp not nil?)]
   ["-p" "--port PORT" "The port to run at."
    :default 3000
    :parse-fn #(Integer/parseInt %)]
   ["-s" "--status STATUS" "The http status code to return."
    :default 200
    :parse-fn #(Integer/parseInt %)]])

(defn error-msg [errors] (str "ERRORS: " (str/join \newline errors)))

(defn error-and-exit [msg]
  (binding [*out* *err*]
    (println msg))
  (System/exit 1))

(def base-headers
  {"Access-Control-Allow-Origin" "*"
   "Access-Control-Allow-Methods" "*"
   "Access-Control-Allow-Credentials" "true"
   "Access-Control-Allow-Headers" "authorization,Content-Type"
   "Access-Control-Request-Headers" "*"})

(defn mk-handler [{:keys [body-file port status]}]
  (fn handler [r]
    (if (= (r :request-method) :options)
      {:status 200 :headers base-headers}
      {:status status
       :headers (merge base-headers {"Content-Type" "application/json"})
       :body (slurp body-file)})))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      errors (-> errors error-msg error-and-exit)
      :true (run-jetty (mk-handler options) {:port (:port options)}))))
